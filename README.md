# SABNA: Scalable Accelerated Bayesian Network Analytics

**Authors:**
Subhadeep Karan <skaran@buffalo.edu>,
Matthew Eichhorn <maeichho@buffalo.edu>,
Blake Hurlburt <blakehur@buffalo.edu>,
Grant Iraci <grantira@buffalo.edu>,
Jaroslaw Zola <jaroslaw.zola@hush.com>


## About

SABNA is a software suite of efficient algorithms for Bayesian networks learning.
The main idea behind the toolkit is to combine various data optimization techniques, and advanced parallel algorithms,
to achieve scalable and exact implementaions capable to process the largest data sets.


## Installing

To install the toolkit simply run `build.sh` in the project's root directory.
The script should create the `bin/` folder with all SABNA tools.
The package is implemented in C++14 and it can be compiled using any standard-conforming compiler with support for Boost library.
We tested `g++` >= 4.9 and `clang++` >= 3.6.2 with `Boost >= 1.59.0`.
If you are familiar with the `cmake` toolchain, you can use standard `cmake` options
to customize the installation process.


## Using

SABNA consists of a set of stand-alone tools, each implementing different functionality.
All tools provide an intuitive command line interface. Use `--help` to get a short description of the available CLI options for each tool.
* `sabna-exsl-mpsbuild` precomputes maximal parent sets (MPS) using selected scoring functiom (currently MDL or BDeu) for any categorical data provided in a csv file.
* `sabna-exsl-astar-ope` finds optimal network structure for a given MPS using A-star combined with a simple heuristic and our optimal path extension technique.
* `sabna-exsl-bfs-ope` finds optimal network structure for a given MPS using the standard BFS with our optimal path extension technique. This is currently the most efficient BN search method.
* `sabna-exsl-bfs` finds optimal network structure for a given MPS using the standard BFS. This method is for comparison purposes only, and should not be used in practice.
* `sabna-order2dag` converts input ordering into the corresponding DAG written in bif or sif format.

### Example

The example below shows how to learn an exact BN under the MDL score using BFS with the optimal path extension technique (see `run-test.sh`).
We use an example binary dataset `autos.csv` with 26 variables and 159 observations (available in `data/`).
1. First, enumerate MPS: `./sabna-exsl-mpsbuild --csv-file autos.csv --mps-file autos.mps --score mdl`
2. Use the resulting MPS to find an optimal ordering: `./sabna-exsl-bfs-ope --n 26 --mps-file autos.mps`
3. Convert the resulting ordering into the corresponding structure in bif format: `./sabna-order2dag --n 26 --csv-file autos.csv --mps-file autos.mps --net-name autos --type bif --order 24 8 23 21 4 13 16 25 17 9 11 19 7 15 3 20 22 12 5 6 0 10 1 2 14 18`


## License

SABNA is released under the MIT License. Some parts of the code are covered by the Boost Software License.


## References

To cite the SABNA package simply refer to this repository. To cite the optimal path extension technique you should refer to our 2016 IEEE BigData
paper: *S. Karan, J. Zola, "Exact Structure Learning of Bayesian Networks by Optimal Path Extension," In Proc. IEEE Int. Conference on Big Data (IEEE BigData), 2016*.
The paper is available from arXiv: https://arxiv.org/abs/1608.02682. To cite our parent set identification method refer to our 2017 IEEE HiPC paper:
*S. Karan, J. Zola, "Scalable Exact Parent Sets Identification in Bayesian Networks Learning with Apache Spark," In Proc. IEEE Int. Conference on High Performance Computing, Data, and Analytics (HiPC), 2017*.
The paper is available from arXiv: https://arxiv.org/abs/1705.06390.
