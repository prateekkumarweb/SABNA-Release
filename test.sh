#!/bin/bash

bin/sabna-exsl-mpsbuild --csv-file data/autos.csv --mps-file autos.mps --score mdl
ORDER=`bin/sabna-exsl-bfs-ope --n 26 --mps-file autos.mps | grep "optimal order:" | sed -e 's/optimal order: //'`
bin/sabna-order2dag --n 26 --csv-file data/autos.csv --mps-file autos.mps --net-name autos --type bif --order $ORDER
