#!/bin/bash

clear
DIR=`pwd`

mkdir -p build/
rm -rf build/*
cd build/

cmake ../ -DCMAKE_INSTALL_PREFIX=$DIR

make -j5 install
#make -j5 install VERBOSE=1
