/***
 *  $Id$
 **
 *  File: sabn-order2dag.cpp
 *  Created: Aug 22, 2017
 *
 *  Author: Subhadeep Karan <skaran@buffalo.edu>
 *          Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2017 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#include <iostream>
#include <numeric>

#include <boost/program_options.hpp>

#include "bit_util.hpp"
#include "config.hpp"
#include "csv.hpp"
#include "MPSList.hpp"
#include "ptable.hpp"

template<int N>
struct variable {
    int range;
    uint_type<N> pa;
}; // struct variable

template <int N>
std::pair<bool, std::string> load_mps_build_network(const std::string& csv_name, const std::string& mps_name, const std::vector<int>& order, std::vector<variable<N>>& var_list) {
    using set_type = uint_type<N>;
    // this is not quite correct
    // type of input data probably should be independent of scoring function
    // we may have different scoring function with different type requirements
    std::vector<signed char> D;

    int n = -1;
    int m = -1;
    bool st;
    std::tie(st, n, m) = read_csv(csv_name, D);

    if (st == false) { return {false, "could not read input data"}; }
    if (n != var_list.size()) { return {false, "too few variables in mps file"}; }

    ptable<N> pt = create_ptable<N>(n, m, std::begin(D));
    set_type pa = set_empty<set_type>();

    std::ifstream f(mps_name);
    if (!f) { return {false, "could not read mps"}; }

    MPSList<N> mps_list;
    auto res = mps_list.read(n, mps_name);

    if (!res.first) { return res; }

    for (auto& xi : order) {
        var_list[xi].range = pt.r(xi);
        var_list[xi].pa = mps_list.find(xi, pa).pa;
        pa = set_add(pa, xi);
    }

    return {true, ""};
} // load_mps_build_network

template<int N>
std::vector<std::vector<int>> cpt_row(int xi, const std::vector<variable<N>>& var_list) {
    std::vector<std::vector<int>> row;
    int row_size = 1;

    std::vector<int> pa_vec = as_vector(var_list[xi].pa);

    for (auto xj : pa_vec) { row_size *= var_list[xj].range; }

    int pa_size = set_size(var_list[xi].pa);
    std::vector<int> temp(pa_size, 0);
    row.push_back(temp);

    int j = pa_size - 1;

    for (int i = 0; i < (row_size - 1); ++i) {
        for (j = pa_size - 1; j >= 0; --j ) {
            if (temp[j] != (var_list[pa_vec[j]].range - 1)) break;
        }
        temp[j] += 1;
        std::fill( std::begin(temp)+ j + 1, std::end(temp), 0);
        row.push_back(temp);
    }

    return row;
} // cpt_row


std::string comma_sep_vec(const std::vector<std::string>& vec) {
    std::string temp = "";
    for (auto& x : vec) { temp += x + ", "; }
    temp.resize(temp.size() - 2);
    return temp;
} // comma_sep_vec


std::string trunc_decimal(double val) {
    val *= 100;
    return "0." + std::to_string(static_cast<int>(val));
} // trunc_decimal


template <typename T>
std::vector<std::string> to_string_vector(const std::vector<T>& list) {
    std::vector<std::string> temp;
    for (auto& x : list) { temp.push_back(std::to_string(x)); }
    return temp;
} // to_string_vector

template<int N>
bool write_bif(const std::string& name, const std::vector<variable<N>>& var_list) {
    std::cout << "writing to " << name << ".bif" << std::endl;

    int n = var_list.size();
    std::ofstream of(name + ".bif");
    if (!of) { return false; }

    of << "network " << name << " {" << std::endl;
    of << "}" << std::endl;

    for (int xi = 0; xi < n; ++xi) {
        of << "variable " << xi << " {" << std::endl;
        of << "    " << "type discrete [ " << var_list[xi].range << " ] { ";
        std::vector<int> temp(var_list[xi].range, 0);
        std::iota(std::begin(temp), std::end(temp), 0);
        of << comma_sep_vec(to_string_vector(temp)) << " }" << std::endl;
        of << "}" << std::endl;
    }

    for (int xi = 0; xi < n; ++xi) {
        std::string val = trunc_decimal(1.0 / var_list[xi].range);
        std::vector<std::string> prob(var_list[xi].range, val);

        of << "probability ( " << xi ;

        //if (var_list[xi].pa == 0) {
        if (is_emptyset<N>(var_list[xi].pa)) {
            of << " ) {" << std::endl;
            of << "    " << "table " << comma_sep_vec(prob) << ";" << std::endl;
            of << "}" << std::endl;
        } else {
            of << " | " << comma_sep_vec(to_string_vector(as_vector(var_list[xi].pa))) << " ) {" << std::endl;

            for (auto& row : cpt_row(xi, var_list)) {
                of << "    " << "( " << comma_sep_vec(to_string_vector(row)) << " ) " << comma_sep_vec(prob) << ";" << std::endl;
            }

            of << "}" << std::endl;
        } // if xi is root
    } // for xi

    of.close();
    std::cout << "done!" << std::endl;

    return true;
} // write_bif

template<int N>
bool write_sif(const std::string& name, const std::vector<variable<N>>& var_list) {
    std::cout << "writing to " << name << ".sif" << std::endl;

    int n = var_list.size();
    std::ofstream of(name + ".sif");
    if (!of) { return false; }

    for (int xi = 0; xi < n; ++xi) {
        //if (var_list[xi].pa != 0) {
        if (!is_emptyset<N>(var_list[xi].pa)) {
            auto pa = as_vector(var_list[xi].pa);
            for (auto p : pa) of << p << " pp " << xi << std::endl;
        } else of << xi << " pp " << xi << std::endl;
    }

    of.close();
    std::cout << "done!" << std::endl;

    return true;
} // write_sif

template <int N>
std::pair<bool, std::string> load_build_write(int n, const std::string& csv_name, const std::string& mps_name, const std::vector<int>& order, const std::string& type, const std::string& out) {
    std::vector<variable<N>> var_list(n);

    auto res = load_mps_build_network(csv_name, mps_name, order, var_list);
    if (!res.first) return res;

    if (type == "bif") {
        if (!write_bif(out, var_list)) { return {false, "unable to create output file"}; }
    } else {
        if (!write_sif(out, var_list)) { return {false, "unable to create output file"}; }
    }

    return {true, ""};
} // load_build_write

int main (int argc, char* argv[]) {
    score_message("sabna-exsl-order2dag");

    int n;
    std::string csv_name;
    std::string mps_name;
    std::string net_name;
    std::string type;
    std::vector<int> order;

    boost::program_options::options_description desc("options");
    desc.add_options()
        ("n", boost::program_options::value<int>(&n)->required(), "number of variables")
        ("csv-file", boost::program_options::value<std::string>(&csv_name)->required(),
         "input data file, column-wise without header, white space separated")
        ("mps-file", boost::program_options::value<std::string>(&mps_name)->required(), "input mps file")
        ("net-name", boost::program_options::value<std::string>(&net_name)->required(), "output network name")
        ("order", boost::program_options::value<std::vector<int>>(&order)->multitoken(),
         "ordering")
        ("type", boost::program_options::value<std::string>(&type)->default_value("sif"), "output network format bif|sif")
        ("help", "this help message")
        ;

    boost::program_options::positional_options_description p;
    p.add("n", 1);
    p.add("csv-file", 1);
    p.add("mps-file", 1);
    p.add("net-name", 1);
    p.add("order", -1);

    boost::program_options::variables_map vm;

    try {
        boost::program_options::store(boost::program_options::command_line_parser(argc, argv).
                                      options(desc).positional(p).run(), vm);

        if (vm.count("help") || argc == 1) {
            std::cout << "usage: " << argv[0] << " [options]" << std::endl;
            std::cout << desc;
            return -1;
        }

        boost::program_options::notify(vm);
    }
    catch (std::exception& e) {
        std::cout << "error: " << e.what() << std::endl;
        return -1;
    }

    if (n != order.size()) {
        std::cout << "error: incorrect ordering, wrong number of variables" << std::endl;
        return -1;
    }

    std::pair<bool, std::string> res = {false, "default"};

    if (n <= 64) res = load_build_write<1>(n, csv_name, mps_name, order, type, net_name);
    else if (n <= 128) res = load_build_write<2>(n, csv_name, mps_name, order, type, net_name);
    else if (n <= 256) res = load_build_write<4>(n, csv_name, mps_name, order, type, net_name);
    else res = load_build_write<5>(n, csv_name, mps_name, order, type, net_name);

    if (!res.first) {
        std::cout << "error: " << res.second << std::endl;
        return -1;
    }

    return 0;
} // main
