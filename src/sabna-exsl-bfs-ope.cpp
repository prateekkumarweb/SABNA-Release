/***
 *  $Id$
 **
 *  File: sabna-exsl-bfs-ope.cpp
 *  Created: Aug 22, 2017
 *
 *  Author: Subhadeep Karan <skaran@buffalo.edu>
 *  Copyright (c) 2017 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstdint>
#include <iostream>
#include <unordered_map>
#include <vector>

#include <boost/program_options.hpp>

#include "bit_util.hpp"
#include "config.hpp"
#include "MPSList.hpp"
#include "jaz/math.hpp"

template<int N>
struct bfs_node {
    double score;
    unsigned char path[64 * N];
};

template<int N>
inline void optimal_path_extension(const MPSList<N>& mps_list, const std::vector<std::pair<uint_type<N>, double>>& opt, uint_type<N>& id, bfs_node<N>& node, int& id_size) {
    int n = mps_list.n();
    bool extend = true;

    for (int i = 0; i < n && extend; ++i) {
        extend = false;
        for (int xi = 0; xi < n; ++xi) {
            if (!in_set(id, xi) && is_superset(id, opt[xi].first)) {
                extend = true;
                id = set_add(id, xi);
                node.score += opt[xi].second;
                node.path[id_size++] = xi;
            }
        } // for xi
    } // for i

} // optimal_path_extension

template<int N>
void bfs_core(const MPSList<N>& mps_list, const std::vector<std::pair<uint_type<N>, double>>& opt, const uint_type<N>& source_id, bfs_node<N> source_node, const uint_type<N>& target_id, bfs_node<N>& target_node, int source_id_size) {
    using set_type = uint_type<N>;

    int n = mps_list.n();

    std::vector<std::unordered_map<uint_type<N>, bfs_node<N>, uint_hash>> v_nl(n+1);

    jaz::Binom<int> binom;   
    int max_init_reserve = 1 << 20; 
    for (int l = source_id_size; l < n; ++l) { 
        v_nl[l].reserve( (max_init_reserve) < binom(n, l) ? (max_init_reserve) : binom(n, l)); 
    }
    
    v_nl[source_id_size].insert({source_id, source_node});

    uint64_t node_counter = 0;
    for (int l = source_id_size; l <= n; ++l) {
        if (v_nl[l].size() == 0) continue;
        else if ((v_nl[l].size() == 1) && (std::begin(v_nl[l])->first == target_id)) { 
                target_node = std::begin(v_nl[l])->second;
                return;
        }
 
        uint64_t node_l_counter = 0;
        for (const auto& pa_node : v_nl[l]) {
            ++node_l_counter;   

            for (int xi = 0; xi < n; ++xi) {
                if (in_set(pa_node.first, xi) || !in_set(target_id, xi)) continue;

                bfs_node<N> ch_node = pa_node.second;
                set_type ch_id = set_add(pa_node.first, xi);
                ch_node.score += mps_list.find(xi, pa_node.first).s;
                int ch_id_size = l;
                ch_node.path[ch_id_size++] = xi;                
                
                optimal_path_extension(mps_list, opt, ch_id, ch_node, ch_id_size);
                
                auto it = v_nl[ch_id_size].find(ch_id);
                if (it != v_nl[ch_id_size].end()) {
                    if (it->second.score > ch_node.score) it->second = ch_node;
                    else continue;
                }
                else { v_nl[ch_id_size].insert({ch_id, ch_node}); }
            } // for xi

            if (node_l_counter % 100000 != 0) continue;
            std::cout << "\t nodes explored in the current layer: " << node_l_counter << std::endl;
        } // for pa_node

        v_nl[l].clear();

        node_counter += node_l_counter;
        if (l < n) std::cout << "processing done at layer: " << l << " : size of next layer is: " << v_nl[l+1].size() << std::endl;
    }

} // bfs_core

template<int N>
std::pair<bool, std::string> bfs(const MPSList<N>& mps_list) {
    using set_type = uint_type<N>;

    int n = mps_list.n();
    set_type E = set_empty<set_type>();
    set_type X = set_full<set_type>(n);

    std::vector<std::pair<set_type, double>> opt(n);
    for (int xi = 0; xi < n; ++xi) {
        auto res = mps_list.optimal(xi);
        opt[xi] = {res.pa, res.s};
    }

    std::unordered_map<set_type, std::pair<int, set_type>, uint_hash> ht_path;

    bfs_node<N> source_node;
    source_node.score = 0;
    for (auto& x : source_node.path) { x = 0; }

    bfs_node<N> target_node;
    target_node.score = -1;
    bfs_core(mps_list, opt, E, source_node, X, target_node, 0);

    if (target_node.score == -1) { return {false, "unable to find any optimal structure"}; }

    std::cout << std::endl << "done!" << std::endl;
    std::cout << std::endl << "optimal score: " << target_node.score << std::endl;
    
    std::cout << "optimal order: ";
    for (int i = 0; i < n; ++i) std::cout << static_cast<int>(target_node.path[i]) << " ";
    std::cout << std::endl << std::endl;
    
    return {true, ""};
} // bfs

template<int N>
std::pair<bool, std::string> read_search(int n, const std::string& in) {
    MPSList<N> mps_list;
    auto res = mps_list.read(n, in);

    if (!res.first) { return res; }

    return bfs(mps_list);
} // read_search

int main (int argc, char* argv[]){
    score_message("sabna-exsl-bfs-ope");

    int n;
    std::string mps_name;

    boost::program_options::options_description desc("options");
    desc.add_options()
        ("mps-file", boost::program_options::value<std::string>(&mps_name)->required(),
         "input mps file")
        ("n", boost::program_options::value<int>(&n)->required(),
         "number of variables")
        ("help", "this help message")
        ;

    boost::program_options::positional_options_description p;
    p.add("mps-file", 1);
    p.add("n", 1);

    boost::program_options::variables_map vm;

    try {
        boost::program_options::store(boost::program_options::command_line_parser(argc, argv).
                                      options(desc).positional(p).run(), vm);

        if (vm.count("help") || argc == 1) {
            std::cout << "usage: " << argv[0] << " [options]" << std::endl;
            std::cout << desc;
            return -1;
        }

        boost::program_options::notify(vm);
    }
    catch (std::exception& e) {
        std::cout << "error: " << e.what() << std::endl;
        return -1;
    }

    std::cout << "input: " << mps_name << std::endl;
    std::cout << "variables: " << n << std::endl;

    auto start = std::chrono::system_clock::now();

    std::pair<bool, std::string> res;
    
    if (n <= 64) res = read_search<1>(n, mps_name);
    else if (n <= 128) res = read_search<2>(n, mps_name);
    else if (n <= 256) res = read_search<4>(n, mps_name);
    else res = read_search<5>(n, mps_name);

    if (!res.first) {
        std::cout << res.second << std::endl;
        return -1;
    }

    auto end = std::chrono::system_clock::now();
    auto diff = end - start;
    std::cout << "time: " << std::chrono::duration_cast<std::chrono::seconds>(diff).count() << "s " << std::endl << std::endl;

    return 0;
} // main
