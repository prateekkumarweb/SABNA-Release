/***
 *  $Id$
 **
 *  File: sabna-exsl-mpsbuild.cpp
 *  Created: Aug 22, 2017
 *
 *  Author: Subhadeep Karan <skaran@buffalo.edu>
 *  Copyright (c) 2017 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#include <algorithm>
#include <chrono>
#include <iostream>
#include <memory>

#include <boost/program_options.hpp>

#include "AICEngine.hpp"
#include "BDeuEngine.hpp"
#include "MDLEngine.hpp"
#include "SL_MPS.hpp"
#include "config.hpp"
#include "csv.hpp"

struct Functors {
    AIC aic;
    MDL mdl;
    BDeu bdeu;
};

template <int N>
void build_and_write(int n, int m, std::vector<signed char>& D, std::string in, std::string mps_name, const std::string& scoring_f, Functors& func) {
    ptable<N> pt = create_ptable<N>(n, m, std::begin(D));

    SL_MPS<N> sl_mps;

    if (scoring_f == "aic") sl_mps.run(AICEngine<N>(pt, func.aic));
    else if (scoring_f == "mdl") sl_mps.run(MDLEngine<N>(pt, func.mdl));
    else sl_mps.run(BDeuEngine<N>(pt, func.bdeu));

    auto res = sl_mps.write(in, mps_name);

    if (!res.first) {
        std::cout << "error: " << res.second << std::endl;
        return;
    }
} // build_and_write

int main (int argc, char* argv[]){
    score_message("sabna-exsl-mpsbuild");

    std::vector<std::string> vec_score{"aic", "mdl", "bdeu"};
    std::string csv_name;
    std::string mps_name;
    std::string scoring_f;
    double alpha;

    boost::program_options::options_description desc("options");
    desc.add_options()
        ("csv-file", boost::program_options::value<std::string>(&csv_name)->required(),
         "input data file, column-wise without header, white space separated")
        ("mps-file", boost::program_options::value<std::string>(&mps_name)->required(), "output mps file")
        ("score,s", boost::program_options::value<std::string>(&scoring_f)->default_value(vec_score[1]),
         "scoring function aic|mdl|bdeu")
        ("alpha,a", boost::program_options::value<double>(&alpha)->default_value(1.0),
         "bdeu hyper-parameter")
        ("help", "this help message")
        ;

    boost::program_options::positional_options_description p;
    p.add("csv-file", 1);
    p.add("mps-file", 1);

    boost::program_options::variables_map vm;

    try {
        boost::program_options::store(boost::program_options::command_line_parser(argc, argv).
                                      options(desc).positional(p).run(), vm);

        if (vm.count("help") || argc == 1) {
            std::cout << "usage: " << argv[0] << " [options]" << std::endl;
            std::cout << desc;
            return -1;
        }

        boost::program_options::notify(vm);
    }
    catch (std::exception& e) {
        std::cout << "error: " << e.what() << std::endl;
        return -1;
    }

    int n;
    int m;
    std::vector<signed char> D;

    bool st;
    std::tie(st, n, m) = read_csv(csv_name, D);

    if (st == false) {
        std::cout << "error: could not read input data" << std::endl;
        return -1;
    }

    std::cout << "input: " << csv_name << std::endl;
    std::cout << "variables: " << n << std::endl;
    std::cout << "instances: " << m << std::endl;
    std::cout << "output: " << mps_name << std::endl;

    auto start = std::chrono::system_clock::now();

    // initialize functors based on provided and default hyper-parameters
    Functors func;

    func.aic = AIC();
    func.mdl = MDL(m);
    func.bdeu = BDeu(alpha);

    if (n > 256) build_and_write<5>(n, m, D, csv_name, mps_name, scoring_f, func);
    else if (n > 128) build_and_write<4>(n, m, D, csv_name, mps_name, scoring_f, func);
    else if (n > 64) build_and_write<2>(n, m, D, csv_name, mps_name, scoring_f, func);
    else build_and_write<1>(n, m, D, csv_name, mps_name, scoring_f, func);

    std::cout << std::endl << "done!" << std::endl;

    auto end = std::chrono::system_clock::now();
    auto diff = end - start;
    std::cout << "time: " << std::chrono::duration_cast<std::chrono::seconds>(diff).count() << "s" << std::endl << std::endl;

    return 0;
} // main
