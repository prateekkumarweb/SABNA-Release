/***
 *  $id$
 **
 *  File: sabna-exsl-astar-ope.cpp
 *  Created: Aug 22, 2017
 *
 *  Author: Subhadeep Karan <skaran@buffalo.edu>
 *  Copyright (c) 2017 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#include <chrono>
#include <iomanip>
#include <iostream>
#include <queue>
#include <unordered_set>
#include <unordered_map>
#include <utility>
#include <vector>

#include <boost/heap/fibonacci_heap.hpp>
#include <boost/program_options.hpp>

#include "bit_util.hpp"
#include "config.hpp"
#include "csv.hpp"
#include "MPSList.hpp"

template<int N>
struct astar_node {
    uint_type<N> id;
    int size;
    double f;
    unsigned char path[64 * N];
}; // astar_node

template<int N>
bool operator< (const astar_node<N>& lhs, const astar_node<N>& rhs) {
    if (lhs.f < rhs.f) return false;
    if (lhs.f > rhs.f) return true;
    if (set_size(lhs.size) > set_size(rhs.size)) return true;
    return false;
} // operator<

template<int N>
void optimal_path_extension(const MPSList<N>& mps_list, const std::vector<std::pair<uint_type<N>, double>>& opt, astar_node<N>& node) {
    int n = mps_list.n();
    bool extend = true;

    for (int i = 0; i < n && extend; ++i) {
        extend = false;
        for (unsigned char xi = 0; xi < n; ++xi) {
            if (!in_set(node.id, xi) && is_superset(node.id, opt[xi].first)) {
                extend = true;
                node.id = set_add(node.id, xi);
                node.path[node.size++] = xi;
            }
        } // for xi
    } // for i

} // optimal_path_extension

template<int N>
void a_star_core(const MPSList<N>& mps_list, const std::vector<std::pair<uint_type<N>, double>>& opt, const astar_node<N>& source_node, astar_node<N>& target_node) {
    using set_type = uint_type<N>;
    using heap_type = boost::heap::fibonacci_heap<astar_node<N>>;
   
    int n = mps_list.n();

    heap_type Q;
    std::unordered_map<set_type, typename heap_type::handle_type, uint_hash> Qidx;
    std::unordered_set<set_type, uint_hash> C;

    Qidx.insert({source_node.id, Q.push(source_node)}); 

    uint64_t counter = 0;
    while(!Q.empty()) {
        astar_node<N> pa_node = Q.top();
        Q.pop();
        Qidx.erase(pa_node.id);
        C.insert(pa_node.id);

        if (pa_node.id == target_node.id) { 
            target_node = pa_node;
            break;
        }
         
        for (int xi = 0; xi < n; ++xi) {
            if (in_set(pa_node.id, xi) || !in_set(target_node.id, xi)) continue;

            astar_node<N> ch_node = pa_node;
            ch_node.id = set_add(ch_node.id, xi); 
            ch_node.f += mps_list.find(xi, pa_node.id).s - opt[xi].second;
            ch_node.path[ch_node.size++] = xi;          
 
            optimal_path_extension(mps_list, opt, ch_node);
            
            if (C.find(ch_node.id) != C.end()) continue;
            
            auto it_Qidx = Qidx.find(ch_node.id);
            if (it_Qidx != Qidx.end()) {
                if ((*(it_Qidx->second)).f > ch_node.f) { 
                    *(it_Qidx->second) = ch_node;
                    Q.update_lazy(it_Qidx->second);
                }
                else continue;
            }
            else Qidx.insert({ch_node.id, Q.push(ch_node)});
        } // for xi

        ++counter;
        if (counter % 200000 != 0)  { continue; }
        std::cout << "nodes explored | current size of PQ: " << counter << " | " << Q.size() << std::endl;
    } // while Q.empty
    
} // a_star_core

template<int N>
std::pair<bool, std::string> a_star(const MPSList<N>& mps_list) {
    using set_type = uint_type<N>;
    using heap_type = boost::heap::fibonacci_heap<astar_node<N>>;
    
    int n = mps_list.n();
    set_type E = set_empty<set_type>();
    set_type X = set_full<set_type>(n);

    // h - heuristic
    double h = 0;
    std::vector<std::pair<set_type, double>> opt(n);
    for (int xi = 0; xi < n; ++xi) {
        auto res = mps_list.optimal(xi);
        opt[xi] = {res.pa, res.s};
        h += res.s;
    }
    
    astar_node<N> source_node;
    source_node.id = E;
    source_node.f = h;
    source_node.size = 0;
    for (auto& x : source_node.path) { x = 0; }

    astar_node<N> target_node;
    target_node.id = X;
    target_node.f = -1;
    
    a_star_core(mps_list, opt, source_node, target_node);  

    if (target_node.f == -1) { return {false, "unable to find any optimal structure"}; }
 
    std::cout << std::endl << "done!" << std::endl;
    std::cout << std::endl << "optimal score: " << target_node.f << std::endl;
    std::cout << "optimal order: ";
    for (int i = 0; i < n; ++i) std::cout << static_cast<int>(target_node.path[i]) << " ";
    std::cout << std::endl;
    
    return {true, ""};
} // a_star

template<int N>
std::pair<bool, std::string> read_search(int n, const std::string& in) {
    MPSList<N> mps_list;
    auto res = mps_list.read(n, in);
    
    if (!res.first) { return res; } 
 
    return a_star(mps_list);
} // read_search

int main (int argc, char* argv[]) {
    score_message("sabna-exsl-astar-ope");
    
    int n;
    std::string mps_name;
    
    boost::program_options::options_description desc("options");
    desc.add_options()
        ("mps-file", boost::program_options::value<std::string>(&mps_name)->required(),
         "input mps file")
        ("n", boost::program_options::value<int>(&n)->required(),
         "number of variables")
        ("help", "this help message")
        ;

    boost::program_options::positional_options_description p;
    p.add("mps-file", 1);
    p.add("n", 1);

    boost::program_options::variables_map vm;

    try {
        boost::program_options::store(boost::program_options::command_line_parser(argc, argv).
                                      options(desc).positional(p).run(), vm);

        if (vm.count("help") || argc == 1) {
            std::cout << "usage: " << argv[0] << " [options]" << std::endl;
            std::cout << desc;
            return -1;
        }

        boost::program_options::notify(vm);
    }
    catch (std::exception& e) {
        std::cout << "error: " << e.what() << std::endl;
        return -1;
    }

    auto start = std::chrono::system_clock::now();

    std::pair<bool, std::string> res;

    if (n <= 64) res = read_search<1>(n, mps_name);
    else if (n <= 128) res = read_search<2>(n, mps_name);
    else if (n <= 256) res = read_search<4>(n, mps_name);
    else res = read_search<5>(n, mps_name);
    
    if (!res.first) {
        std::cout << res.second << std::endl;
        return -1;
    }

    auto end = std::chrono::system_clock::now();
    auto diff = end - start;
    std::cout << "time: " << std::chrono::duration_cast<std::chrono::seconds>(diff).count() << "s" << std::endl << std::endl;

    return 0;
} // main
