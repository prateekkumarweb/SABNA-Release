/***
 *    $Id$
 **
 *    File: ptable.hpp
 *    Created: Oct 22, 2016
 *
 *    Authors: Matthew Eichhorn <maeichho@buffalo.edu>
 *             Blake Hurlburt <blakehur@buffalo.edu>
 *             Grant Iraci <grantira@buffalo.edu>
 *    Copyright (c) 2015-2017 SCoRe Group http://www.score-group.org/
 *    Distributed under the MIT License.
 *    See accompanying file LICENSE.
 */

#ifndef PTABLE_HPP
#define PTABLE_HPP

#include "bitvector.hpp"
#include "bit_util.hpp"
#include <vector>
#include <cmath>


template <int N> class ptable {
public:
    typedef uint_type<N> set_type;

    explicit ptable(int n = 0, int m = 0) : n_(n), m_(m) {}

    int n() const { return n_; }

    int m() const { return m_; }

    int r(int i) const { return data_[i].first.size(); }

    // assume that q will not overflow, this will be checked by sabna calling code
    int q(const set_type& pa) const {
        int q = 1;

        for (int i = 0; i < set_max_size<set_type>(); ++i) {
            if (in_set(pa, i)) {
                q *= r(i);
            }
        }

        return q;
    }

    template <typename score_functor>
    void apply(const set_type& xi, const set_type& pa, std::vector<score_functor>& F) const {

        std::vector<int> xi_vect = as_vector(xi);

        for (int i = 0; i < F.size(); ++i) {
            F[i].init(r(xi_vect[i]), q(pa));
        }

        if (is_emptyset(pa)) {
            for (int i = 0; i < F.size(); ++i) {
                F[i](m_);
                for (const vect& v : data_[xi_vect[i]].first) {
                    F[i](v.weight(), m_);
                }
                F[i].finalize(1);
            }
            return;
        }

        using Iter = typename std::vector<vect>::const_iterator;

        std::vector<int> pa_sorted;

        for (int i : sorted_order_) {
            if (in_set(pa, i)) {
                pa_sorted.push_back(i);
            }
        }

        //the table stores pairs with
        //first = current bitvector at that level of the dfs traversal
        //second = iterator over bitvectors of next parent to intersect with
        std::vector<std::pair<vect, Iter>> table;
        table.reserve(pa_sorted.size());

        for (int i : pa_sorted) {
            table.push_back(std::make_pair(vect::identity(m_), std::begin(data_[i].first))); //start with identity
        }

        //[0] should be ?
        table.push_back(std::make_pair(vect::identity(m_), std::begin(data_[0].first)));
        table.push_back(std::make_pair(vect::identity(m_), std::begin(data_[0].first)));

        int layer = 0; //we start at the top of the tree
        int qi_obs = 0;

        vect container = vect::identity(m_); //bottom layer

        while (true) { //once we are finished, we will try to move one layer up from root

            while (layer >= 0 && table[layer].second == std::end(data_[pa_sorted[layer]].first)) {
                //reset the iterator for next traversal at this level
                table[layer].second = std::begin(data_[pa_sorted[layer]].first);
                --layer; //move up to continue traversal
            }

            if (layer < 0) break;

            const vect& root = table[layer].first; //fetch from the table

            intersect(root, *(table[layer].second), table[layer + 1].first);
            ++table[layer].second;

            int Nij = table[layer + 1].first.weight();

            if (Nij) { //if Nijk is zero we just move on

                if (layer == (pa_sorted.size() - 1)) { //we have explored all of the parents
                    for (int i = 0; i < F.size(); ++i) F[i](Nij);
                    ++qi_obs;
                    for (int i = 0; i < F.size(); ++i) {
                        for (const vect& v : data_[xi_vect[i]].first) {
                            intersect(table[layer + 1].first, v, container);
                            int Nijk = container.weight();
                            if (Nijk) {
                                F[i](Nijk, Nij);
                            }
                        }
                    }
                } else if (Nij == 1) {
                    //if we go down a level, we will find that this is the only call that would be made
                    for (int i = 0; i < F.size(); ++i) F[i](Nij);
                    ++qi_obs;
                    for (int i = 0; i < F.size(); ++i) {
                        F[i](1,1);
                    }
                } else { //there are more parents
                    ++layer; //move down a layer and keep going
                }
            }
        }

        //uncomment to set qi = number of observed parent set configuration states
        /*
        for (int i = 0; i < F.size(); ++i) {
            F[i].finalize(qi_obs);
        }
        */
    } // apply


private:
    typedef bitvector vect;

    class ent_order {
    public:
        ent_order(const std::vector<std::pair<std::vector<vect>, double>>& data) : data_(data) { }

        bool operator()(int lhs, int rhs) {
            return data_[lhs].second < data_[rhs].second;
        }

    private:
        const std::vector<std::pair<std::vector<vect>, double>>& data_;
    };

    std::vector<std::pair<std::vector<vect>, double>> data_;
    std::vector<int> sorted_order_;
    int n_ = -1;
    int m_ = -1;

    template <int M, typename Iter>
    friend ptable<M> create_ptable(int, int, Iter);

    template <int M>
    friend ptable<M> reorder_ptable(ptable<M>&, const std::vector<int>&);

}; // class ptable


template <int N, typename Iter> ptable<N> create_ptable(int n, int m, Iter it) {
    ptable<N> p(n, m);

    int indices[256];
    int temp;
    int size;

    for (; n > 0; --n) {
        p.data_.push_back(std::make_pair(std::vector<typename ptable<N>::vect>(), 0.0));

        size = 0;
        std::fill_n(indices, 256, -1);

        for (int j = 0; j < m; ++j) {
            temp = *it++;

            if (indices[temp] == -1) {
                indices[temp] = size++;
                p.data_.back().first.push_back(typename ptable<N>::vect(m));
            }

            p.data_.back().first[indices[temp]].insert(j);
        }

        double H = 0.0;
        for (auto& v : p.data_.back().first) {
            double px = (static_cast<double>(v.weight()) / m);
            H += px * (px == 0.0 ? 0 : std::log2(px));
        }
        //std::cout << p.data_.size() << ":" << H <<std::endl;
        p.data_.back().second = -H;
    }

    for (int i = 0; i < p.data_.size(); ++i) {
        p.sorted_order_.push_back(i);
    }

    std::sort(std::begin(p.sorted_order_), std::end(p.sorted_order_), typename ptable<N>::ent_order(p.data_));

    return p;
} // create_ptable


template <int N> ptable<N> reorder_ptable(ptable<N>& A, const std::vector<int>& order) {
    ptable<N> p(A.n(), A.m());

    p.data_ = std::vector<std::pair<std::vector<typename ptable<N>::vect>, double>>();
    p.data_.reserve(A.n());
    p.sorted_order_.reserve(A.n());

    for (int i : order) {
        p.data_.emplace_back(std::move(A.data_[i]));
    }

    // A is no longer valid because we used std::move
    A.data_ = {};
    A.sorted_order_ = {};
    A.m_ = -1;
    A.n_ = -1;

    for (int i = 0; i < p.data_.size(); ++i) {
        p.sorted_order_.push_back(i);
    }

    std::sort(std::begin(p.sorted_order_), std::end(p.sorted_order_), typename ptable<N>::ent_order(p.data_));

    return p;
} // reorder_ptable


#endif // PTABLE_HPP
