/***
 *  $Id$
 **
 *  File: MDLEngine.hpp
 *  Created: Aug 18, 2017
 *
 *  Author: Subhadeep Karan <skaran@buffalo.edu>
 *  Copyright (c) 2017 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef MDL_ENGINE_HPP
#define MDL_ENGINE_HPP

#include "MDL.hpp"
#include "ScoringEngine.hpp"
#include "bit_util.hpp"
#include "ptable.hpp"


template <int N>
class MDLEngine : public ScoringEngine<N> {
public:
    using set_type = uint_type<N>;
    using score_type = MDL::score_type;

    MDLEngine(ptable<N>& pt, MDL& mdl) : ScoringEngine<N>(pt.n(), pt.m()), pt_(pt), mdl_(mdl) {
        n_ = pt_.n();
        m_ = pt_.m();

        E_ = set_empty<set_type>();
        X_ = set_full<set_type>(n_);

        H_.resize(n_, -1);
        md_.resize(n_, X_);
        norder_.resize(n_, -1);

        m_init_entropy__();
        m_reorder_ptable__();
        m_max_pa_size__();
    } // MDLEngine

    int n() const { return n_; }

    set_type process(set_type pa, set_type ch, MPSList<N>& mps_list, int l) {
        std::vector<MDL> vec_mdl(set_size(ch), mdl_);
        pt_.apply(ch, pa, vec_mdl);
        for (int xi = 0, idx = 0; xi < n_; ++xi) {
            if (!in_set(ch, xi)) continue;
            auto score = vec_mdl[idx].score();
            if (!extend_insert(xi, pa, score, mps_list)) ch = set_remove(ch, xi);
            ++idx;
        }
        ch = ch & md_[l + 1];
        return ch;
    } // process

    bool extend_insert(int xi, set_type pa, const MDL::score_type& score, MPSList<N>& mps_list) {
        auto res = mps_list.find(xi, pa);
        if (score.first < res.s) { return mps_list.insert(xi, pa, score.first); }
        if (score.second + H_[xi] >= res.s) { return false; }
        return true;
    } // extend_and_insert

    void finalize(MPSList<N>& mps_list) {
        mps_list.map_variables(norder_);
    } // finalize

private:
    void m_init_entropy__() {
        std::vector<MDL> vec_mdl(1, mdl_);
        for (int xi = 0; xi < n_; ++xi) {
            pt_.apply(set_add(E_, xi), set_remove(X_, xi), vec_mdl);
            auto res = vec_mdl[0].score();
            H_[xi] = res.first - res.second;
        }
    } // m_init_entropy__

    void m_reorder_ptable__() {
        struct PNode {
            int xi;
            int ri;
            double H;

            bool operator<(const PNode& rhs) const {
                if (ri != rhs.ri) return ri > rhs.ri;
                return H < rhs.H;
            }
        }; // struct PNode

        std::vector<PNode> v(n_);
        std::vector<MDL> vec_mdl;
        vec_mdl.resize(1, mdl_);

        for (int xi = 0; xi < n_; ++xi) {
            pt_.apply(set_add(E_, xi), set_remove(X_, xi), vec_mdl);
            auto res = vec_mdl[0].score();
            v[xi] = PNode{xi, pt_.r(xi), res.first - res.second};
        }

        std::sort(std::begin(v), std::end(v));

        for (int i = 0; i < n_; ++i) {
            norder_[i] = v[i].xi;
            H_[i] = v[i].H;
        }

        pt_ = reorder_ptable(pt_, norder_);
    } // m_reorder_ptable__

    void m_max_pa_size__() {
        std::vector<MDL> vec_mdl(n_, mdl_);
        pt_.apply(X_, E_, vec_mdl);
    
        for (int xi = n_ - 1; xi >= 0; --xi) {
            double thres = vec_mdl[xi].score().first - H_[xi];
            double val = vec_mdl[xi].score().second;

            int l = 0;
            for (int xj = n_ - 1; xj >= 0 && val <= thres; --xj) {
                if (xi == xj) continue;
                val *= pt_.r(xj);
                l += 1;
            }
            for (; l < n_; ++l) md_[l] = set_remove(md_[l], xi);
        }
    } // m_max_pa_size__


    int n_ = -1;
    int m_ = -1;

    ptable<N>& pt_;

    set_type X_;
    set_type E_;

    std::vector<double> H_;
    std::vector<int> norder_;
    std::vector<set_type> md_;

    MDL mdl_;

}; // class MDLEngine

#endif // MDL_ENGINE_HPP
