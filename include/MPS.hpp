/***
 *  $Id$
 **
 *  File: MPS.hpp
 *  Created: Mar 21, 2017
 *
 *  Author: Subhadeep Karan <skaran@buffalo.edu>
 *  Copyright (c) 2017 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef MPS_HPP
#define MPS_HPP

#include <iostream>
#include <fstream>
#include <vector>

#include "ScoringEngine.hpp"

template <int N>
class MPS {
public:
    using set_type = uint_type<N>;
    using MPSNode = typename MPSList<N>::MPSNode;

    std::pair<bool, std::string> write(const std::string& dataset_name, const std::string& mps_name) {
        std::ofstream f(mps_name);
        if (!f) return std::make_pair(false, "mps write error");
        f << mps_list;
        return std::make_pair(true, "");
    } // write

    std::pair<bool, std::string> read(const std::string& csv_name) {
        std::ifstream f(csv_name);
        if (!f) return std::make_pair(false, "mps read error");
        f >> mps_list;
        return std::make_pair(true, "");
    } // read

protected:
    void init(int n) { mps_list.init(n); }

    MPSList<N> mps_list;

}; // class MPS

#endif // MPS
