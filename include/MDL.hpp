/***
 *  $Id$
 **
 *  File: MDL.hpp
 *  Created: Nov 22, 2016
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2016 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef MDL_HPP
#define MDL_HPP

#include <cmath>

class MDL {
public:
    typedef std::pair<double, double> score_type;

    MDL() { }

    explicit MDL(int m) : logm_(std::log2(m)) { }

    void init(int ri, int qi) { score_ = 0.0; ri_ = ri; qi_ = qi; }

    void finalize(int qi) { qi_ = qi; }

    void operator()(int Nij) { }

    void operator()(int Nijk, int Nij) {
        double p = static_cast<double>(Nijk) / Nij;
        score_ += (Nijk * std::log2(p));
    } // operator()

    int r() const { return ri_; }

    score_type score() const {
        double nc = logm_ * (ri_ - 1) * qi_ * 0.5;
        return std::make_pair(-1.0 * (score_ - nc), nc);
    } // score


private:
    double score_ = 0.0;
    double logm_;

    int ri_ = 1;
    int qi_ = 1;

}; // class MDL

#endif // MDL_HPP
