/***
 *  $Id$
 **
 *  File: BDeuEngine.hpp
 *  Created: Aug 18, 2017
 *
 *  Author: Subhadeep Karan <skaran@buffalo.edu>
 *  Copyright (c) 2017 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef BDeu_ENGINE_HPP
#define BDeu_ENGINE_HPP

#include "BDeu.hpp"
#include "ScoringEngine.hpp"
#include "bit_util.hpp"
#include "ptable.hpp"


template <int N>
class BDeuEngine : public ScoringEngine<N> {
public:
    using set_type = uint_type<N>;
    using score_type = BDeu::score_type;

    BDeuEngine(ptable<N>& pt, BDeu& bdeu) : ScoringEngine<N>(pt.n(), pt.m()), pt_(pt), bdeu_(bdeu) {
        n_ = pt_.n();
        m_ = pt_.m();

        E_ = set_empty<set_type>();
        X_ = set_full<set_type>(n_);

        md_.resize(n_, X_);
    } // BDeuEngine

    int n() const { return n_; }

    set_type process(set_type pa, set_type ch, MPSList<N>& mps_list, int l) {
        std::vector<BDeu> vec_bdeu(set_size(ch), bdeu_);
        pt_.apply(ch, pa, vec_bdeu);
        for (int xi = 0, idx = 0; xi < n_; ++xi) {
            if (!in_set(ch, xi)) continue;
            if (!extend_insert(xi, pa, vec_bdeu[idx].score(), mps_list)) ch = set_remove(ch, xi);
            ++idx;
        }
        ch = ch & md_[l + 1];
        return ch;
    } // process

    bool extend_insert(int xi, set_type pa, const BDeu::score_type& score, MPSList<N>& mps_list) {
        auto res = mps_list.find(xi, pa);
        if (std::get<0>(score) < res.s) { return mps_list.insert(xi, pa, std::get<0>(score)); }
        if (std::get<1>(score) <= pruning_constant && res.s < std::get<2>(score)) { return false; }
        return true;
    } // extend_and_insert

    void finalize(MPSList<N>& mps_list) {
    } // finalize


private:
    int n_ = -1;
    int m_ = -1;

    BDeu bdeu_;

    ptable<N>& pt_;

    set_type X_;
    set_type E_;

    std::vector<BDeu> vec_bdeu_;
    std::vector<int> norder_;
    std::vector<set_type> md_;

    // de_campos pruning constant
    // for further info refer: Efficient Structure Learning of Bayesian Networks using Constraints by de Campos
    // link: goo.gl/FxJghq
    const double pruning_constant = 0.8349;

}; // class BDeuEngine

#endif // BDeu_ENGINE_HPP
