/***
 *  $Id$
 **
 *  File: SL_MPS.hpp
 *  Created: June 4, 2017
 *
 *  Author: Subhadeep Karan <skaran@buffalo.edu>
 *  Copyright (c) 2017 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef SL_MPS_HPP
#define SL_MPS_HPP

#include <algorithm>
#include <string>
#include <unordered_map>
#include <vector>

#include "MPS.hpp"

template <int N>
class SL_MPS : public MPS<N> {
public:
    using set_type = typename MPS<N>::set_type;

    template<typename Engine>
    void run(Engine score) {
        n_ = score.n();
        MPS<N>::init(n_);

        E_ = set_empty<set_type>();
        X_ = set_full<set_type>(n_);

        std::vector<Task> cl, nl;
        std::vector<std::pair<Task, int>> B;

        nl.push_back({E_, X_});

        int l = 0;

        while (!nl.empty()) {
            cl.swap(nl);
            nl.resize(0);

            B.resize(0);

            uint64_t counter = 0;
            for (auto& tk : cl) {
                tk.ch_ = score.process(tk.pa_, tk.ch_, MPS<N>::mps_list, l);
                if (!is_emptyset(tk.ch_)) m_fill_next_layer__(tk.pa_, tk.ch_, nl);
                else B.push_back({Task{tk.pa_, tk.ch_}, nl.size()});
                if ((++counter) % 10000 != 0) { continue; }
                std::cout << "\t computed score for " << counter << " nodes out of " << cl.size() << std::endl; 
            }

            if (!B.empty() && !nl.empty()) m_prune_next_layer__(B, nl);
            ++l;
            std::cout << "exploration done for all variables at layer " << l - 1 << std::endl;
        }

        score.finalize(MPS<N>::mps_list);
    } // run

private:
    struct Task {
        set_type pa_;
        set_type ch_; 
    };

    void m_fill_next_layer__(set_type pa, set_type ch, std::vector<Task>& nl);
    void m_prune_next_layer__(const std::vector<std::pair<Task, int>>& B, std::vector<Task>& nl);

    int n_;

    set_type X_;
    set_type E_;

}; // class SL_MPS

template <int N>
inline void SL_MPS<N>::m_fill_next_layer__(set_type pa, set_type ch, std::vector<Task>& nl) {
    int xi_max = n_ - 1;
    for (; xi_max >= 0 && !in_set(pa, xi_max); --xi_max);
    for (int xi = xi_max + 1; xi < n_; ++xi) nl.push_back({set_add(pa, xi), set_remove(ch, xi)});
} // m_fill_next_layer__

template <int N>
inline void SL_MPS<N>::m_prune_next_layer__(const std::vector<std::pair<Task, int>>& B, std::vector<Task>& nl) {
    std::unordered_map<set_type, std::pair<set_type, int>, uint_hash> ht;
    ht.reserve(B.size());

    auto it_ht = ht.begin();
    for (const auto& nd : B) {
        int xi_max = n_ - 1;
        for (; xi_max >= 0 && !in_set(nd.first.pa_, xi_max); --xi_max);
        for (int xi = xi_max - 1; xi >= 0; --xi) {
            if (in_set(nd.first.pa_, xi)) continue;
            set_type npa = set_add(nd.first.pa_, xi);
            it_ht = ht.find(npa);
            if (it_ht == ht.end()) {
                ht.insert({npa, {nd.first.ch_, nd.second}});
                continue;
            }
            else {
                it_ht->second.first = it_ht->second.first & nd.first.ch_;
                it_ht->second.second = it_ht->second.second < nd.second ? it_ht->second.second : nd.second;
            }
        }
    }

    auto it_nl = nl.begin();
    for (const auto& kv : ht) {
        set_type npa = kv.first;
        it_nl  = std::find_if(nl.begin(), nl.begin() + kv.second.second, [npa](const Task& tk) { return npa == tk.pa_; });
        if (it_nl != nl.end() && !is_emptyset(it_nl->ch_)) { it_nl->ch_ = it_nl->ch_ & kv.second.first; }
    }
} // m_prune_next_layer__

#endif // SL_MPS_HPP
