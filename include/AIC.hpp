/***
 *  $Id$
 **
 *  File: AIC.hpp
 *  Created: Nov 22, 2016
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2016-2017 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef AIC_HPP
#define AIC_HPP

#include <cmath>

class AIC {
public:
    typedef std::pair<double, double> score_type;

    void init(int ri, int qi) { score_ = 0.0; ri_ = ri; qi_ = qi; }

    void finalize(int qi) { qi_ = qi; }

    void operator()(int Nij) { }

    void operator()(int Nijk, int Nij) {
        double p = static_cast<double>(Nijk) / Nij;
        score_ += (Nijk * std::log2(p));
    } // operator()

    int r() const { return ri_; }

    score_type score() const {
        double nc = (ri_ - 1) * qi_;
        return std::make_pair(-1.0 * (score_ - nc), nc);
    } // score


private:
    double score_ = 0.0;

    int ri_ = 1;
    int qi_ = 1;

}; // class AIC

#endif // AIC_HPP
