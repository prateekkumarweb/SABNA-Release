/***
 *  $Id$
 **
 *  File: ScoreEngine.hpp
 *  Created: Aug 18, 2017
 *
 *  Author: Subhadeep Karan <skaran@buffalo.edu>
 *  Copyright (c) 2017 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef SCORING_ENGINE_HPP
#define SCORING_ENGINE_HPP

#include "MPSList.hpp"
#include "bit_util.hpp"


template <int N>
class ScoringEngine {
public:
    using set_type = uint_type<N>;

    ScoringEngine(int n, int m) : n_(n), m_(m) {}

    virtual ~ScoringEngine() { }

    int n() const { return n_; }

    int m() const { return m_; }

protected:
    int n_ = -1;
    int m_ = -1;

}; // class ScoringEngine

#endif // SCORING_ENGINE_HPP
