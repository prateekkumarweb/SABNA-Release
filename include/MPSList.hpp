/***
 *  $Id$
 **
 *  File: MPSList.hpp
 *  Created: May 24, 2017
 *
 *  Author: Subhadeep Karan <skaran@buffalo.edu>
 *  Copyright (c) 2017 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef MPSLIST_HPP
#define MPSLIST_HPP

#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <random>
#include <string>
#include <tuple>
#include <vector>

#include "bit_util.hpp"

template<int N>
class MPSList;

template<int N>
std::istream& operator>>(std::istream& in, MPSList<N>& obj);

template<int N>
std::ostream& operator<<(std::ostream& out, const MPSList<N>& obj);


template<int N>
class MPSList {
public:
    using set_type = uint_type<N>;

    struct MPSNode {
        double s;
        set_type pa;
    }; // struct MPSNode

    int n() const { return n_; }

    void init(int n) { n_ = n; mps_list.resize(n_, std::vector<MPSNode>{}); }
    
    const MPSList::MPSNode& optimal(int xi) const { return mps_list[xi].back();  }

    const MPSNode& find(int xi, set_type u) const {
        int idx = mps_list[xi].size() - 1;
        for (; idx > 0 && !is_superset(u, mps_list[xi][idx].pa); --idx);
        if (idx >= 0) return mps_list[xi][idx];
        return temp_mps_;
    } // find

    const MPSNode& find_sample(int xi, set_type u, const double rand_val) const {
        std::vector<int> vec_idx;

        for (int i = mps_list[xi].size() - 1; i >= 0; --i) {
            if (!is_superset(u, mps_list[xi][i].pa)) { continue; }
            vec_idx.push_back(i);
        }

        return mps_list[xi][static_cast<int>(rand_val * vec_idx.size())];
    } // find
    
    bool insert(int xi, set_type u, double s) {
        auto it = std::upper_bound(std::begin(mps_list[xi]), std::end(mps_list[xi]), MPSNode{s, set_type{0}}, [](const MPSNode& x, const MPSNode& y) { return x.s > y.s; });
        mps_list[xi].insert(it, MPSNode{s, u});
        return true;
    } // insert

    bool insert(int xi, set_type u, double s,int idx) {
        while (idx > 0 && mps_list[xi][idx].s < s) --idx;
        mps_list[xi].insert(std::begin(mps_list[xi])+idx, MPSNode{s, u});
        return true;
    } // insert

    void erase(int xi, set_type u) {
        auto it = std::begin(mps_list[xi]);
        auto it_end = std::end(mps_list[xi]);
        for (; it != it_end && it->pa != u; ++it);
        if (it != it_end) mps_list[xi].erase(it);
    } // erase

    std::vector<set_type> adjacency() const {
        std::vector<set_type> adj(n_, set_empty<set_type>());
        for (int xi = 0; xi < n_; ++xi) {
            for (auto& nd : mps_list[xi]) {
                for (auto& xj : as_vector(nd.pa)) adj[xj] = set_add(adj[xj], xi);
            }
        }
        return adj;
    } // adjacency

    friend std::istream& operator>>(std::istream& in, MPSList<N>& obj) {
        std::string str = "";
        while (in && (in >> str)) if (str.find(obj.starts) != std::string::npos) break;

        set_type E = set_empty<set_type>();

        obj.mps_list.clear();
        int xi = -1;
        double s = -1;
        int npa = -1;

        while (in) {
            int t = 0;
            in >> str;
            if (str.find(obj.ends) != std::string::npos) break;
            in >> t >> s >> npa;
            if (xi != t) obj.mps_list.push_back(std::vector<MPSNode>());
            xi = t;
            set_type pa = E;
            int xj = -1;
            for (int i = 0; i < npa; ++i) { in >> xj; pa = set_add(pa, xj); }
            obj.mps_list[xi].push_back(MPSNode{s, pa});
        }
        obj.n_ = obj.mps_list.size();

        return in;
    } // operator>>

    void map_variables(const std::vector<int>& order) {
        std::vector<std::vector<MPSNode>> temp(n_);

        for (int i = n_ - 1; i >= 0; --i) {
            temp[order[i]] = std::move(mps_list[i]);
            for (auto& node : temp[order[i]]) {
                set_type npa = set_empty<set_type>();
                for (int xi = 0; xi < n_; ++xi) {
                    if (in_set(node.pa, xi)) { npa = set_add(npa, order[xi]); }
                }
                node.pa = npa;
            }
        }

        std::swap(mps_list, temp);
    } // map_variables

    friend std::ostream& operator<<(std::ostream& out, const MPSList<N>& obj) {
        std::string str = obj.starts + "\n";

        for (int xi = 0; xi < obj.n_; ++xi) {
            for (auto& nd : obj.mps_list[xi]) {
                str += "mps " + std::to_string(xi) + " " + std::to_string(nd.s) + " " + std::to_string(set_size(nd.pa)) + " ";
                for (auto& xj : as_vector(nd.pa)) str += std::to_string(xj) + " ";
                str += "\n";
            }
        }

        str += obj.ends + "\n";
        return out << str;
    } // operator<<

    std::pair<bool, std::string> read(int n, const std::string& in) {
        std::ifstream f(in);
        if (!f) return {false, "could not read mps file"};
        f >> *this;
        if (n_ != n) return {false, "insufficient number of variables found in the supplied mps file"};
        return {true, ""};
    } // read

    std::pair<bool, std::string> write(const std::string& out) {
        std::ofstream f(out);
        if (!f) return {false, "could not write mps file"};
        f << *this;
        return {true, ""};
    } // write


private:
    const double INF = std::numeric_limits<double>::max();
    const MPSNode temp_mps_{INF, set_empty<set_type>()};
    std::vector<std::vector<MPSNode>> mps_list;

    int n_ = -1;

    const std::string starts = "MPSList_Begins";
    const std::string ends = "MPSList_Ends";

}; // class MPSList

#endif // MPSLIST_HPP
